import turtle 
import time  
from random import *
from pydub import AudioSegment
from pydub.playback import play 
import threading



def music():
	sound=AudioSegment.from_file("234.mp3", format="mp3")
	song1 = sound[30000:120000]
	play(song1)


def main():
	thread=threading.Thread(target=music)
	thread.start()

	
	
	

	##	设置窗口，画笔
	turtle.setup(1100,800)
	turtle.pensize(3)
	turtle.speed(2)
	turtle.hideturtle()
	turtle.bgpic(r'1.gif')

	turtle.penup()
	turtle.goto(-525,-400)
	turtle.left(60)
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.pendown()
	turtle.circle(-100,120)
	 
	turtle.left(170)
	turtle.circle(-1000,16)


	turtle.left(85)
	turtle.forward(70)

	turtle.left(89)
	turtle.circle(-1000,16)
	turtle.end_fill()
	turtle.penup()
	##定位到下边长方形右下角的点
	turtle.goto(-410,-137)
	turtle.right(160)
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.pendown()
	turtle.forward(50)

	turtle.left(87)
	turtle.forward(67)

	turtle.left(90)
	turtle.forward(50)
	turtle.end_fill()

	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.backward(50)
	turtle.right(93)
	turtle.forward(10)
	turtle.right(90)
	turtle.forward(30)
	
	turtle.right(91)
	turtle.forward(11)

	turtle.right(87)
	turtle.forward(31)

	turtle.left(93)
	turtle.forward(67)
	
	turtle.left(4)
	turtle.forward(10)

	turtle.left(90)
	turtle.forward(30)
	
	turtle.left(91)
	turtle.forward(11)

	turtle.left(87)
	turtle.forward(31)

	turtle.backward(31)
	turtle.right(180)

	turtle.left(87)
	turtle.forward(68)
	turtle.end_fill()

	
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.backward(8)

	turtle.right(95)
	turtle.forward(170)
	turtle.right(85)
	turtle.forward(25)
	turtle.right(84)
	turtle.forward(170)

	
	##turtle.penup()
	##turtle.goto(-415,-57)
	##turtle.left(11)
	
	##turtle.pendown()
	##turtle.forward(170)
	

	
	##turtle.left(85)
	##turtle.forward(25)
	turtle.end_fill()
	turtle.penup()
	##上边的环状

	turtle.goto(-460,115)
	turtle.right(130)
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.pendown()
	turtle.forward(17)
	##向右转
	

	turtle.right(110)
	turtle.circle(-46,72.5)
	
	##向左转
	turtle.right(99)
	turtle.forward(17)
	turtle.end_fill()
	
	##往回走，往上画
	turtle.penup()
	turtle.goto(-437,132)##第一个环上的右边点 
	turtle.right(135)
	turtle.pendown()
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.forward(30)
	turtle.left(90)
	turtle.forward(20)
	turtle.left(90)
	turtle.forward(30)
	
	
	turtle.end_fill()
	turtle.penup()
	##第二个圆柱
	
	turtle.backward(30)
	turtle.left(90)
	turtle.forward(20)
	turtle.left(60)
	turtle.pendown()
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.circle(50,40)
	turtle.left(80)
	turtle.forward(34)
	turtle.left(78)
	turtle.circle(47,44)
	turtle.end_fill()
	
	##第二个环往上
	turtle.penup()
	turtle.goto(-439,195)
	turtle.left(148)
	turtle.pendown()
	turtle.fillcolor((random(),random(),random()))
	turtle.forward(20)
	turtle.left(90)
	turtle.forward(20)
	turtle.left(90)
	turtle.forward(20)
	turtle.end_fill()
	turtle.penup()
	


	turtle.backward(20)
	turtle.left(90)
	turtle.forward(7.5)
	turtle.left(90)
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.pendown()
	turtle.forward(20)
	turtle.right(90)
	turtle.forward(5)
	turtle.right(90)
	turtle.forward(20)
	turtle.end_fill()
	turtle.penup()

	turtle.backward(22)
	turtle.right(90)
	turtle.forward(2.5)
	turtle.right(88)
	turtle.pendown()
	turtle.pencolor('#FF0000')
	turtle.forward(20)
##sun

	
	turtle.speed(25)
	turtle.penup()

	turtle.goto(-385,250)
	turtle.pensize(1)
	turtle.color("red", "yellow")
	turtle.pendown()
	turtle.begin_fill()
	for _ in range(37):
		turtle.forward(150)
		turtle.left(170)
	turtle.end_fill()
	
	for _ in range(13):
		turtle.forward(150)
		turtle.left(170)

	
	

####スカイツリー
	turtle.speed(2)
	turtle.penup()
	
	turtle.pencolor('black')
	turtle.pensize(3)
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#7
	turtle.goto(355,-390)
	turtle.pendown()
	turtle.goto(365,-260)
	turtle.goto(485,-260)
	#8
	turtle.goto(495,-390)
	#7
	turtle.goto(355,-390)
	turtle.penup()
	turtle.end_fill()


	
	
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.goto(365,-260)
	turtle.pendown()
	turtle.goto(375,-130)
	turtle.goto(475,-130)
	turtle.goto(485,-260)
	turtle.goto(365,-260)
	turtle.penup()
	turtle.end_fill()

	
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.goto(375,-130)
	turtle.pendown()
	#1
	turtle.goto(385,0)
	#2
	turtle.goto(465,0)
	turtle.goto(475,-130)
	turtle.goto(375,-130)
	turtle.penup()
	turtle.end_fill()

	
	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#2
	turtle.goto(465,0)
	turtle.pendown()
	#5
	turtle.goto(370,0)
	#3
	turtle.goto(350,50)
	#4
	turtle.goto(500,50)
	#6
	turtle.goto(480,0)
	#2
	turtle.goto(465,0)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#3
	turtle.goto(350,50)
	turtle.pendown()
	#9
	turtle.goto(350,60)
	#10
	turtle.goto(500,60)
	#4
	turtle.goto(500,50)
	#3
	turtle.goto(350,50)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#11
	turtle.goto(395,60)
	turtle.pendown()
	#13
	turtle.goto(400,160)
	#14
	turtle.goto(445,160)
	#12
	turtle.goto(450,60)
	#11
	turtle.goto(395,60)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#13
	turtle.goto(400,160)
	turtle.pendown()
	#15
	turtle.goto(390,160)
	#17
	turtle.goto(390,180)
	#18
	turtle.goto(455,180)
	#16
	turtle.goto(455,160)
	#13
	turtle.goto(400,160)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#19
	turtle.goto(410,180)
	turtle.pendown()
	#21
	turtle.goto(410,220)
	#22
	turtle.goto(435,220)
	#20
	turtle.goto(435,180)
	#19
	turtle.goto(410,180)
	turtle.penup()
	turtle.end_fill()

	

	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#23
	turtle.goto(420,220)
	turtle.pendown()
	#25
	turtle.goto(420,340)
	#26
	turtle.goto(425,340)
	#24
	turtle.goto(425,220)
	#23
	turtle.goto(420,220)
	turtle.penup()
	turtle.end_fill()


	#200
	turtle.speed(10)
	turtle.goto(375,30)
	turtle.begin_fill()
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(390,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(405,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(420,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(435,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(450,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()

	turtle.goto(465,30)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(8)
		turtle.right(90)
	turtle.penup()
	turtle.end_fill()




	#400
	turtle.speed(2)
	turtle.goto(419,240)
	turtle.pendown()
	turtle.goto(426,240)
	turtle.penup()

	turtle.goto(419,260)
	turtle.pendown()
	turtle.goto(426,260)
	turtle.penup()
	
	turtle.goto(419,280)
	turtle.pendown()
	turtle.goto(426,280)
	turtle.penup()

	turtle.goto(419,300)
	turtle.pendown()
	turtle.goto(426,300)
	turtle.penup()

	turtle.goto(419,320)
	turtle.pendown()
	turtle.goto(426,320)
	turtle.penup()

	turtle.goto(419,340)
	turtle.pendown()
	turtle.goto(426,340)
	turtle.penup()



	







	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#1
	turtle.goto(-232,-390)
	turtle.pendown()
	#2
	turtle.goto(-232,-235)
	#3
	turtle.goto(232,-235)
	#4
	turtle.goto(232,-390)
	#5
	turtle.goto(215,-390)
	#6
	turtle.goto(215,-350)
	#7
	turtle.goto(147,-350)
	#8
	turtle.goto(147,-390)
	#9
	turtle.goto(130,-390)
	#10
	turtle.goto(130,-235)
	#11
	turtle.goto(62,-235)
	#12
	turtle.goto(62,-390)
	#13
	turtle.goto(45,-390)
	#14
	turtle.goto(45,-235)
	#15
	turtle.goto(-45,-235)
	#16
	turtle.goto(-45,-390)
	#17
	turtle.goto(-62,-390)
	#18
	turtle.goto(-62,-235)
	#19
	turtle.goto(-130,-235)
	#20
	turtle.goto(-130,-390)
	#21
	turtle.goto(-147,-390)
	#22
	turtle.goto(-147,-350)
	#23
	turtle.goto(-215,-350)
	#24
	turtle.goto(-215,-390)
	#1
	turtle.goto(-232,-390)
	turtle.penup()
	turtle.end_fill()





	turtle.fillcolor((random(),random(),random()))
	turtle.goto(147,-333)
	turtle.begin_fill()
	turtle.left(48.5)
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(68)
		turtle.left(90)
	turtle.penup()
	turtle.end_fill()
	
	turtle.goto(-215,-333)
	turtle.begin_fill()
	turtle.pendown()
	for i in range(1,5):
		turtle.forward(68)
		turtle.left(90)
	turtle.penup()
	turtle.end_fill()









	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#2
	turtle.goto(-232,-235)
	turtle.pendown()
	#27
	turtle.goto(-340,-173)
	#28
	turtle.goto(340,-173)
	#3
	turtle.goto(232,-235)
	#2
	turtle.goto(-232,-235)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#2
	turtle.goto(-232,-235)
	turtle.pendown()
	#25
	turtle.goto(-232,-218)
	#26
	turtle.goto(232,-218)
	#3
	turtle.goto(232,-235)
	#2
	turtle.goto(-232,-235)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#27
	turtle.goto(-340,-173)
	turtle.pendown()
	#29
	turtle.goto(-232,-142)
	#30
	turtle.goto(232,-142)
	#28
	turtle.goto(340,-173)
	#27
	turtle.goto(-340,-173)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#29
	turtle.goto(-232,-142)
	turtle.pendown()
	#31
	turtle.goto(-232,-96)
	#32
	turtle.goto(232,-96)
	#30
	turtle.goto(232,-142)
	#29
	turtle.goto(-232,-142)
	turtle.penup()
	turtle.end_fill()


	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#31
	turtle.goto(-232,-96)
	turtle.pendown()
	#33
	turtle.goto(-310,-10)
	#34
	turtle.goto(310,-10)
	#32
	turtle.goto(232,-96)
	#31
	turtle.goto(-232,-96)
	turtle.penup()
	turtle.end_fill()




	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#33
	turtle.goto(-310,-10)
	turtle.pendown()
	#35
	turtle.goto(-232,36)
	#36
	turtle.goto(232,36)
	#34
	turtle.goto(310,-10)
	#33
	turtle.goto(-310,-10)
	turtle.penup()
	turtle.end_fill()




	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	#37
	turtle.goto(-20,-96)
	turtle.pendown()
	#38
	turtle.goto(-20,-32)
	#39
	turtle.goto(20,-32)
	#40
	turtle.goto(20,-96)
	#37
	turtle.goto(-20,-96)
	turtle.penup()
	turtle.end_fill()

	turtle.goto(0,-64)
	turtle.write("浅",font=("Times New Roman",17,"normal"),align="center")
	turtle.goto(0,-80)
	turtle.write("草",font=("Times New Roman",17,"normal"),align="center")
	turtle.goto(0,-96)
	turtle.write("寺",font=("Times New Roman",17,"normal"),align="center")

	turtle.fillcolor((random(),random(),random()))
	turtle.begin_fill()
	turtle.penup()
	turtle.goto(26.5,-250)
	turtle.left(120)
	turtle.pendown()
	turtle.circle(30,120)
	turtle.left(29.5)
	turtle.forward(55)
	turtle.left(30)
	turtle.circle(30,120)
	turtle.goto(26.5,-250)
	turtle.end_fill()
	turtle.penup()

	turtle.goto(0,-263)
	turtle.write("小",font=("Times New Roman",27,"normal"),align="center")
	turtle.goto(0,-291)
	turtle.write("舟",font=("Times New Roman",27,"normal"),align="center")
	turtle.goto(0,-319)
	turtle.write("町",font=("Times New Roman",27,"normal"),align="center")





### atsumaru
	turtle.speed(2)
	turtle.goto(-300,350)
	turtle.pensize(20)
	turtle.pencolor("#B3EE3A")
	turtle.pendown()
	turtle.goto(-200,350)
	turtle.right(60)
	turtle.backward(50)
    
	turtle.goto(-250,360)
	turtle.right(90)
    
	turtle.backward(10)
	turtle.goto(-250,300)
	turtle.goto(-225,250)

	turtle.penup()
	turtle.goto(-225,330)
	turtle.pendown()
	turtle.goto(-275,250)
	turtle.goto(-300,250)
	turtle.goto(-300,300)
	turtle.goto(-200,300)
	turtle.goto(-200,250)

	turtle.penup()
	turtle.goto(-125,325)
	turtle.pendown()
	turtle.goto(0,325)
	turtle.goto(0,250)
	turtle.goto(-100,250)
	turtle.penup()

	turtle.goto(75,355)
	turtle.pendown()
	turtle.goto(175,355)
	turtle.penup()
	turtle.goto(75,320)
	turtle.pendown()
	turtle.goto(175,320)
	turtle.penup()
	turtle.goto(125,375)
	turtle.pendown()
	turtle.goto(125,250)
	turtle.goto(75,250)
	turtle.goto(75,285)
	turtle.goto(175,285)
	turtle.goto(175,250)
	turtle.penup()
	turtle.goto(250,360)
	turtle.pendown()
	turtle.goto(350,360)
	turtle.goto(300,330)
	turtle.goto(350,310)
	turtle.goto(350,250)
	turtle.goto(250,250)
	turtle.goto(250,290)
	turtle.goto(300,290)
	turtle.goto(300,250)
	turtle.penup()




	turtle.pencolor((random(),random(),random()))
	turtle.goto(0,70)
	turtle.write("大好き!",font=("Times New Roman",130,"normal"),align="center")

	




 
	 
	 



main()
input()